import 'package:bogdan_redux_practice/main.dart';
import 'package:flutter/material.dart';

class AppState {
  final List<Item> items;

  const AppState({
    @required this.items,
  });

  AppState.initialState() : items = List.unmodifiable(<Item>[]);

  AppState.fromJson(Map json)
      : items = (json['items'] as List).map((i) => Item.fromJson(i)).toList();

  Map toJson() => {'items' : items};
}


class CounterState {
  final List<Item> items;

  const CounterState({
    @required this.items,
  });

  CounterState.initialState() : items = List.unmodifiable(<Item>[]);

  CounterState.fromJson(Map json)
      : items = (json['items'] as List).map((i) => Item.fromJson(i)).toList();

  Map toJson() => {'items' : items};
}

class GalleryState {

  final List<Item> items;

  const GalleryState({
    @required this.items,
  });

  GalleryState.initialState() : items = List.unmodifiable(<Item>[]);

  GalleryState.fromJson(Map json)
      : items = (json['items'] as List).map((i) => Item.fromJson(i)).toList();

  Map toJson() => {'items' : items};
}





